const tabsWrapper = document.querySelector(".tabs");
const tabsElements = document.querySelectorAll(".tabs li");
const tabsContent = document.querySelectorAll(".tabs-content li");

tabsWrapper.addEventListener("click", function (e) {
  tabsElements.forEach(tab => {
    tab.classList.remove("active");
  })

  if (e.target.classList.contains("active")) {
    e.target.classList.remove("active");
  } else {
    e.target.classList.add("active");
  }

  contentChanger(e.target.textContent);
});

function contentChanger (tabName) {
  tabsContent.forEach(tab => {
    tab.classList.remove("active");

    if (tab.dataset.content === tabName) {
      console.log(tab.dataset.content)
      tab.classList.add("active");
    }
  })
}
